package businessLayer;

import model.Account;
import model.Person;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Radu Bompa
 * 5/10/2018
 */
public interface BankProc {
	/**
	 * Create a new account
	 * @param account - to be created
	 * @pre account != null;
	 * @post bank.getAccount(@return) != null;
	 */
	int addAccount(Account account);

	/**
	 * Retrieve an account by id
	 * @param accountId - the id of the account to be retrieved
	 * @return - the requested account
	 */
	Account getAccount(int accountId);

	/**
	 * Get all the accounts in the DB
	 * @return - all the accounts
	 */
	Map<String, Account> getAllAccounts();

	/**
	 * Update a chosen account
	 * @param account - to be modified
	 * @pre account != null;
	 * @pre account.getId() > 0;
	 * @post bank.getAccount(account.getId).equals(account);
	 */
	void editAccount(Account account);

	/**
	 * Remove an account
	 * @param accountId - the id of the account to be deleted
	 * @pre accountId > 0;
	 * @post bank.getAccount(accountId) == null;
	 */
	void deleteAccount(int accountId);

	/**
	 * Create a new person
	 * @param person - person to be created
	 * @pre person != null;
	 * @post bank.getPerson(@return) != null;
	 */
	int addPerson(Person person);

	/**
	 * Get a person by id
	 * @param personId - the id of the person to be retrieved
	 * @return - the requested person or null
	 */
	Person getPerson(int personId);

	/**
	 * Get all persons
	 * @return - a list of all persons in db
	 */
	Set<Person> getAllPersons();

	/**
	 * Modify a person parameters (except id)
	 * @param person - person with the parameters to be updated
	 * @pre person != null;
	 * @post bank.getPerson().equals(person);
	 */
	void editPerson(Person person);

	/**
	 * Remove a person
	 * @param personId - the id of the person to be removed
	 * @pre personId > 0;
	 */
	void deletePerson(int personId);

	/**
	 * Deposit money into an account
	 * @param account - the account in which we deposit
	 * @param money - the  amount to deposit
	 * @pre account != null;
	 * @post bank.getAccount(account.getId).getBalance() == account.getBalance() + money;
	 * @return new account balance
	 */
	double depositMoney(Account account, double money);

	/**
	 * Withdraw money from an account
	 * @param account - the account in which we deposit
	 * @param money - the  amount to withdraw
	 * @pre account != null;
	 * @post bank.getAccount(account.getId()).getBalance() == account.getBalance() - money;
	 * @return new account balance
	 */
	double withdrawMoney(Account account, double money);
}
