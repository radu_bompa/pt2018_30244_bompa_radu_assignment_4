package businessLayer;

import model.Account;
import model.Person;
import model.SavingAccount;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Radu Bompa
 * 5/10/2018
 */
public class Bank extends Observable implements BankProc, Serializable {

	private static Set<Person> persons = new LinkedHashSet<>();
	private static Map<String, Account> accountMap = new LinkedHashMap<>();

	private Set<Person> sPersons = new LinkedHashSet<>();
	private Map<String, Account> sAccountMap = new LinkedHashMap<>();

	private static Bank instance = new Bank();

	private Bank() {
	}

	public static Bank getInstance() {
		return instance;
	}

	/**
	 * Read the bank status from file
	 */
	public void init() {
		try {
			FileInputStream fileIn = new FileInputStream("bank.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			instance = (Bank) in.readObject();
			accountMap = instance.sAccountMap;
			persons = instance.sPersons;
			in.close();
			fileIn.close();
			instance.addObserver(new AccountNotifier());
			assert wellFormed();
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file 'bank.ser'");
		} catch (IOException ex) {
			System.out.println("Error reading file 'bank.ser'");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write the bank status to file
	 */
	public void close() {
		try {
			FileOutputStream fileOut = new FileOutputStream("bank.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			Set<Person> finalPersons = new LinkedHashSet<>();
			for (Person person : persons) {
				if (person.getId() != 0 && person.getName() != null) {
					finalPersons.add(person);
				}
			}
			for (Map.Entry<String, Account> entry : accountMap.entrySet()) {
				if (entry.getValue() instanceof SavingAccount) {
					if (((SavingAccount) entry.getValue()).getEndDate().before(new Date()) && ((SavingAccount) entry.getValue()).getInterest() > 0.00) {
						entry.getValue().setBalance(entry.getValue().getBalance() * (1 + ((SavingAccount) entry.getValue()).getInterest()));
						((SavingAccount) entry.getValue()).setInterest(0.00);
					}
				}
			}
			sPersons = finalPersons;
			sAccountMap = accountMap;
			out.writeObject(this);
			out.close();
			fileOut.close();
		} catch (IOException ex) {
			System.out.println("Error writing to file 'bank.ser'");
		}
	}

	/**
	 * Create a new account
	 * @param account - to be created
	 * @return the id of the new account
	 */
	public int addAccount(Account account) {
		assert account != null;
		if (accountMap.isEmpty()) {
			account.setId(1);
		} else {
			account.setId(accountMap.entrySet().stream().max((a1, a2) -> a1.getValue().getId() > a2.getValue().getId() ? 1 : -1).get().getValue().getId() + 1);
		}
		account.setKey(getPerson(account.getHolderId()).getName().hashCode() + "");
		accountMap.put(account.getKey(), account);
		assert getAccount(account.getId()).equals(account);
		assert wellFormed();
		return account.getId();
	}

	/**
	 * Get account by id
	 * @param id -  the id of the account to be retrieved
	 * @return the account
	 */
	public Account getAccount(int id) {
		Optional<Map.Entry<String, Account>> optional = accountMap.entrySet().stream().filter(e -> e.getValue().getId() == id).findFirst();
		return optional.map(Map.Entry::getValue).orElse(null);
	}

	public Map<String, Account> getAllAccounts() {
		return accountMap;
	}

	/**
	 * Modify an account
	 * @param account - to be modified
	 */
	public void editAccount(Account account) {
		assert account != null;
		assert account.getId() > 0;
		deleteAccount(account.getId());
		accountMap.put(account.getKey(), account);
		assert getAccount(account.getId()).equals(account);
		assert wellFormed();
	}

	/**
	 * Remove an account
	 * @param accountId - the id of the account to be deleted
	 */
	public void deleteAccount(int accountId) {
		assert accountId > 0;
		accountMap.remove(getAccount(accountId).getKey());
		assert getAccount(accountId) == null;
		assert wellFormed();
	}

	/**
	 * Create a new person
	 * @param person - person to be created
	 * @return
	 */
	public int addPerson(Person person) {
		assert person != null;
		if (persons.isEmpty()) {
			person.setId(1);
		} else {
			person.setId(persons.stream().max((p1, p2) -> p1.getId() > p2.getId() ? 1 : -1).get().getId() + 1);
		}
		persons.add(person);
		assert getPerson(person.getId()) != null;
		assert wellFormed();
		return person.getId();
	}

	public Person getPerson(int personId) {
		List<Person> personList = persons.stream().filter(p -> p.getId() == personId).collect(Collectors.toList());
		if (!personList.isEmpty()) {
			return personList.get(0);
		}
		return null;
	}

	public Set<Person> getAllPersons() {
		return persons;
	}

	/**
	 * Modify a person profile
	 * @param person - person with the parameters to be updated
	 */
	public void editPerson(Person person) {
		assert person != null;
		assert person.getId() > 0;
		deletePerson(person.getId());
		persons.add(person);
		assert getPerson(person.getId()).equals(person);
		assert wellFormed();
	}

	/**
	 * Remove a person
	 * @param personId - the id of the person to be removed
	 */
	public void deletePerson(int personId) {
		assert personId > 0;
		persons.remove(getPerson(personId));
		assert getPerson(personId) == null;
		assert wellFormed();
	}

	/**
	 * Deposit money into an account
	 * @param account - the account in which we deposit
	 * @param money - the  amount to deposit
	 * @return new account balance
	 */
	public double depositMoney(Account account, double money) {
		assert account != null;
		account.setBalance(account.getBalance() + money);
		notifyObservers(account);
		assert wellFormed();
		return account.getBalance();
	}

	/**
	 * Withdraw money from an account
	 * @param account - the account in which we deposit
	 * @param money - the  amount to withdraw
	 * @return new balance
	 */
	public double withdrawMoney(Account account, double money) {
		assert account != null;
		account.setBalance(account.getBalance() - money);
		setChanged();
		notifyObservers(account);
		assert wellFormed();
		return account.getBalance();
	}

	/**
	 * Return true if <code>validateState</code> does not throw
	 * an IllegalArgumentException, otherwise return false.
	 *
	 * Call at the end of any public method which has changed
	 * state (any "mutator" method). This is usually done in
	 * an assertion, since it corresponds to a post-condition.
	 * For example,
	 * <pre>
	 * assert hasValidState() : this;
	 * </pre>
	 * This method is provided since <code>validateState</code> cannot be used
	 * in an assertion.
	 */
	private boolean wellFormed() {
		boolean result = true;
		try {
			validateState();
		}
		catch (IllegalArgumentException ex){
			result = false;
		}
		return result;
	}

	private void validateState() {
		for (Person person : persons) {
			if (person.getId() <= 0) {
				throw new IllegalArgumentException("Illegal argument - person id: " + person.getId());
			}
			if (person.getName() == null || person.getName().isEmpty()) {
				throw new IllegalArgumentException("Illegal argument - person name: " + person.getName());
			}
		}

		for (Map.Entry<String, Account> entry : accountMap.entrySet()) {
			String key = entry.getKey();
			Account account = entry.getValue();
			if (key == null || key.isEmpty()) {
				throw new IllegalArgumentException("Illegal argument - account key: " + key);
			}
			if (account.getId() <= 0) {
				throw new IllegalArgumentException("Illegal argument - account id: " + account.getId());
			}
			if (account.getHolderId() <= 0) {
				throw new IllegalArgumentException("Illegal argument - account holder id: " + account.getHolderId());
			}
			if (account.getKey() == null || account.getKey().isEmpty()) {
				throw new IllegalArgumentException("Illegal argument - account key: " + account.getKey());
			}
			if (!account.getType().equals("SAVING") && !account.getType().equals("SPENDING")) {
				throw new IllegalArgumentException("Illegal argument - account type: " + account.getType());
			}
		}
	}
}
