package businessLayer;

import model.Account;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Radu Bompa
 * 5/10/2018
 */
public class AccountNotifier implements Observer {
	@Override
	public void update(Observable o, Object arg) {
		if (arg != null) {
			Account account = (Account) arg;
			JOptionPane.showMessageDialog(null,
					"An operation was executed on account no.: " + account.getId() + " hold by " + account.getHolderId()
							+ ". The new account balance is: " + account.getBalance());
		}
	}
}
