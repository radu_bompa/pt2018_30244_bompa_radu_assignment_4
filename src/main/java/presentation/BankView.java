package presentation;

import businessLayer.Bank;
import model.Person;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Created by Radu Bompa
 * 5/10/2018
 */
public class BankView extends MainController {
	public BankView() {
		super();

		JComponent panel = new JPanel();
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		panel.setAlignmentY(JPanel.TOP_ALIGNMENT);
		panel.setLayout(null);
		panel.setBounds(0, 0, 300, 600);

		Container pane = bankFrame.getContentPane();
		pane.add(panel);
		final GridBagConstraints c = new GridBagConstraints();

		Bank bank = Bank.getInstance();

		JButton openBankButton = new JButton("Open bank");
		openBankButton.addActionListener(e -> {
			try {
				bank.init();
				//Add content to the window.
				personFrame.setLayout(null);
				PersonView personView = new PersonView();
				personView.setName("personView");
				personFrame.add(personView);
				personFrame.setSize(new Dimension(300, 600));
				personFrame.setLocation(300, 0);

				//Add content to the window.
				accountFrame.setLayout(null);
				AccountView accountView = new AccountView();
				accountView.setName("accountView");
				accountFrame.add(accountView);
				accountFrame.setSize(new Dimension(600, 600));
				accountFrame.setLocation(600, 0);

				personFrame.repaint();
				personFrame.setVisible(true);
				accountFrame.repaint();
				accountFrame.setVisible(true);
			} catch (Exception ex) {
				System.out.println("Error: " + ex.getMessage());
			}
		});

		openBankButton.setBounds(0, 10, 100, 30);
		panel.add(openBankButton);


		JButton closeBankButton = new JButton("Close bank");
		closeBankButton.addActionListener(e -> {
			try {
				bank.close();
				Component[] components = personFrame.getContentPane().getComponents();
				for (Component component : components) {
					if (component.getName() != null && (component.getName().equals("personView"))) {
						personFrame.getContentPane().remove(component);
						break;
					}
				}
				personFrame.setVisible(false);
				components = accountFrame.getContentPane().getComponents();
				for (Component component : components) {
					if (component.getName() != null && (component.getName().equals("accountView"))) {
						accountFrame.getContentPane().remove(component);
						break;
					}
				}
				accountFrame.setVisible(false);
			} catch (Exception ex) {
				System.out.println("Error: " + ex.getMessage());
			}
		});

		closeBankButton.setBounds(0, 50, 100, 30);
		panel.add(closeBankButton);
		panel.repaint();
		pane.repaint();
	}
}
