package presentation;

import businessLayer.Bank;
import model.Account;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class AccountView extends MainController {
	protected static final JFrame f = new JFrame();//fereastra principala


	public AccountView() {
		super();

		JComponent panel = new JPanel();
		panel.setName("accountView");
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		panel.setAlignmentY(JPanel.TOP_ALIGNMENT);
		panel.setLayout(null);
		panel.setBounds(0, 0, 600, 600);

		Container pane = accountFrame.getContentPane();
		pane.add(panel);
		final GridBagConstraints c = new GridBagConstraints();

		final Bank bank = Bank.getInstance();
		//      account id text
		JLabel accountIdLabel = new JLabel("Id");
		JTextField accountIdText = new JTextField();
		accountIdText.setColumns(30);
		accountIdText.setEditable(false);

		accountIdLabel.setBounds(0, 30, 170, 20);
		panel.add(accountIdLabel);
		accountIdText.setBounds(0, 55, 100, 20);
		panel.add(accountIdText);

		//      account key text
		JLabel accountKeyLabel = new JLabel("key");
		JTextField accountKeyText = new JTextField();
		accountKeyText.setColumns(30);
		accountKeyText.setEditable(false);

		accountKeyLabel.setBounds(0, 75, 170, 20);
		panel.add(accountKeyLabel);
		accountKeyText.setBounds(0, 100, 100, 20);
		panel.add(accountKeyText);

		//      account holder id text
		JLabel accountHolderIdLabel = new JLabel("Holder id");
		java.util.Set<Person> personList = bank.getAllPersons();
		String[] personIds = personList.stream().map(p -> p.getId() + "").collect(Collectors.toList()).toArray(new String[personList.size()]);
		JComboBox personIdSelect = new JComboBox(personIds);
		personIdSelect.setName("personIdSelect");

		accountHolderIdLabel.setBounds(0, 125, 170, 20);
		panel.add(accountHolderIdLabel);
		personIdSelect.setBounds(0, 150, 100, 20);
		panel.add(personIdSelect);

		//      account balance text
		JLabel accountBalanceLabel = new JLabel("Balance");
		JTextField accountBalanceText = new JTextField();
		accountBalanceText.setColumns(30);

		accountBalanceLabel.setBounds(0, 175, 170, 20);
		panel.add(accountBalanceLabel);
		accountBalanceText.setBounds(0, 200, 100, 20);
		panel.add(accountBalanceText);

//		account type
		JLabel accountTypeLabel = new JLabel("Type");
		String[] accountTypes = {"SAVING", "SPENDING"};
		JComboBox accountTypeSelect = new JComboBox(accountTypes);
		accountTypeSelect.setName("accountTypeSelect");

		accountTypeLabel.setBounds(0, 225, 170, 20);
		panel.add(accountTypeLabel);
		accountTypeSelect.setBounds(0, 250, 100, 20);
		panel.add(accountTypeSelect);

		//      account begin time text
		JLabel accountBeginTimeLabel = new JLabel("Begin time");
		JTextField accountBeginTimeText = new JTextField();
		accountBeginTimeText.setColumns(30);

		accountBeginTimeLabel.setBounds(0, 275, 170, 20);
		panel.add(accountBeginTimeLabel);
		accountBeginTimeText.setBounds(0, 300, 100, 20);
		panel.add(accountBeginTimeText);

		//      account end time text
		JLabel accountEndTimeLabel = new JLabel("End time");
		JTextField accountEndTimeText = new JTextField();
		accountEndTimeText.setColumns(30);


		accountEndTimeLabel.setBounds(0, 325, 170, 20);
		panel.add(accountEndTimeLabel);
		accountEndTimeText.setBounds(0, 350, 100, 20);
		panel.add(accountEndTimeText);

		//      account interest text
		JLabel accountInterestLabel = new JLabel("Interest");
		JTextField accountInterestText = new JTextField();
		accountInterestText.setColumns(30);

		accountInterestLabel.setBounds(0, 375, 170, 20);
		panel.add(accountInterestLabel);
		accountInterestText.setBounds(0, 400, 100, 20);
		panel.add(accountInterestText);

//		all accounts table

		java.util.Map<String, Account> accountList;
		if (bank.getAllAccounts().isEmpty()) {
			accountList = new LinkedHashMap<>();
			accountList.put("", new Account());
		} else {
			accountList = bank.getAllAccounts();
		}


		JTable accountsTable = createTable(accountList.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList()));

		//    deposit/withdraw label
		JLabel depositWithdrawLabel = new JLabel("Deposit/withdraw");
		JTextField depositWithdrawText = new JTextField();
		depositWithdrawText.setColumns(30);


		depositWithdrawLabel.setBounds(300, 365, 170, 20);
		panel.add(depositWithdrawLabel);
		depositWithdrawText.setBounds(300, 390, 100, 20);
		panel.add(depositWithdrawText);

//		deposit money button
		JButton depositMoneyButton = new JButton("Deposit");
		depositMoneyButton.addActionListener(e -> {
			try {
				int selectedRow = accountsTable.getSelectedRow();
				Account account = bank.getAccount(Integer.parseInt(accountsTable.getValueAt(accountsTable.getSelectedRow(), 4).toString()));

				bank.depositMoney(account, Double.parseDouble(depositWithdrawText.getText()));
				accountsTable.getModel().setValueAt(account.getBalance() + "", selectedRow, 0);
				panel.repaint();
				panel.repaint();
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
			}
		});

		depositMoneyButton.setBounds(300, 290, 100, 30);
		panel.add(depositMoneyButton);

//		withdraw money button
		JButton withdrawMoneyButton = new JButton("Withdraw");
		withdrawMoneyButton.addActionListener(e -> {
			try {
				int selectedRow = accountsTable.getSelectedRow();
				Account account = bank.getAccount(Integer.parseInt(accountsTable.getValueAt(accountsTable.getSelectedRow(), 4).toString()));
				if (account.getBalance() >= Double.parseDouble(depositWithdrawText.getText())) {
					bank.withdrawMoney(account, Double.parseDouble(depositWithdrawText.getText()));
					accountsTable.getModel().setValueAt(account.getBalance() + "", selectedRow, 0);
					panel.repaint();
				} else {
					JOptionPane.showMessageDialog(null, "Insufficient funds for withdrawal!");
				}
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
			}
		});

		withdrawMoneyButton.setBounds(300, 330, 100, 30);
		panel.add(withdrawMoneyButton);


		accountsTable.getSelectionModel().addListSelectionListener(event -> {
			if (accountsTable.getSelectedRow() >= 0 && accountsTable.getValueAt(accountsTable.getSelectedRow(), 4) != null && accountsTable.getValueAt(accountsTable.getSelectedRow(), 4) != "") {
				Account account = new Account();
				account.setId(Integer.parseInt(accountsTable.getValueAt(accountsTable.getSelectedRow(), 4).toString()));
				account = bank.getAccount(account.getId());
				if (account != null) {
					accountIdText.setText(account.getId() + "");
					accountKeyText.setText(account.getKey() + "");
					int index = 0;
					for (index = 0; index < personIdSelect.getItemCount(); index++) {
						if (Integer.parseInt(personIdSelect.getItemAt(index).toString()) == account.getHolderId()) {
							break;
						}
					}
					personIdSelect.setSelectedIndex(index);
					accountBalanceText.setText(account.getBalance() + "");
					accountTypeSelect.setSelectedIndex(account.getType().equals("SAVING") ? 0 : 1);
					if (account.getType().equals("SAVING")) {
						DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
						accountBeginTimeText.setText(dateFormat.format(((SavingAccount) account).getBeginDate()));
						accountEndTimeText.setText(dateFormat.format(((SavingAccount) account).getEndDate()));
						accountInterestText.setText(((SavingAccount) account).getInterest() + "");
						withdrawMoneyButton.setVisible(false);
						depositMoneyButton.setVisible(false);
						depositWithdrawLabel.setVisible(false);
						depositWithdrawText.setVisible(false);
					} else {
						accountBeginTimeText.setText("");
						accountEndTimeText.setText("");
						accountInterestText.setText("");
						withdrawMoneyButton.setVisible(true);
						depositMoneyButton.setVisible(true);
						depositWithdrawLabel.setVisible(true);
						depositWithdrawText.setVisible(true);
					}
				}
			} else {
				accountIdText.setText("");
				accountKeyText.setText("");
				personIdSelect.setSelectedIndex(0);
				accountBalanceText.setText("");
				accountTypeSelect.setSelectedIndex(0);
				accountBeginTimeText.setText("");
				accountEndTimeText.setText("");
				accountInterestText.setText("");
			}
		});

		JScrollPane sp = new JScrollPane(accountsTable);
		sp.setBounds(0, 420, 600, 160);
		panel.add(sp);

		//      add, edit, delete buttons
		JButton addAccountButton = new JButton("Add account");
		addAccountButton.addActionListener(e -> {
			try {
				Account account = null;
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				if ("SPENDING".equals(accountTypeSelect.getSelectedItem().toString())) {
					account = new SpendingAccount();
					account.setHolderId(Integer.parseInt(personIdSelect.getSelectedItem().toString()));
					account.setBalance(Double.parseDouble(accountBalanceText.getText()));
					account.setType(accountTypeSelect.getSelectedItem().toString());
				} else if ("SAVING".equals(accountTypeSelect.getSelectedItem().toString())) {
					account = new SavingAccount();
					account.setHolderId(Integer.parseInt(personIdSelect.getSelectedItem().toString()));
					account.setBalance(Double.parseDouble(accountBalanceText.getText()));
					account.setType(accountTypeSelect.getSelectedItem().toString());
					((SavingAccount) account).setBeginDate(dateFormat.parse(accountBeginTimeText.getText()));
					((SavingAccount) account).setEndDate(dateFormat.parse(accountEndTimeText.getText()));
					((SavingAccount) account).setInterest(Double.parseDouble(accountInterestText.getText()));
				}
				if (account != null) {
					int accountId = bank.addAccount(account);
					if (account instanceof SavingAccount) {
						((DefaultTableModel) accountsTable.getModel()).addRow(new Object[]{account.getBalance() + "", dateFormat.format(((SavingAccount) account).getBeginDate()), dateFormat.format(((SavingAccount) account).getEndDate()), account.getHolderId() + "", accountId + "", ((SavingAccount) account).getInterest(), account.getKey(), account.getType()});
					} else {
						((DefaultTableModel) accountsTable.getModel()).addRow(new Object[]{account.getBalance() + "", "", "", account.getHolderId() + "", accountId + "", "", account.getKey(), account.getType()});
					}
				}
				panel.repaint();
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
			}
		});

		addAccountButton.setBounds(150, 30, 100, 30);
		panel.add(addAccountButton);

		JButton editAccountButton = new JButton("Edit account");
		editAccountButton.addActionListener(e -> {
			try {
				int selectedRow = accountsTable.getSelectedRow();
				if (accountsTable.getValueAt(selectedRow, 1) != "") {
					Account account = null;
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
					if ("SPENDING".equals(accountTypeSelect.getSelectedItem().toString())) {
						account = new SpendingAccount();
						account.setId(Integer.parseInt(accountIdText.getText()));
						account.setKey(bank.getAccount(account.getId()).getKey());
						account.setHolderId(Integer.parseInt(personIdSelect.getSelectedItem().toString()));
						account.setBalance(Double.parseDouble(accountBalanceText.getText()));
						account.setType(accountTypeSelect.getSelectedItem().toString());
					} else if ("SAVING".equals(accountTypeSelect.getSelectedItem().toString())) {
						account = new SavingAccount();
						account.setId(Integer.parseInt(accountIdText.getText()));
						account.setKey(bank.getAccount(account.getId()).getKey());
						account.setHolderId(Integer.parseInt(personIdSelect.getSelectedItem().toString()));
						account.setBalance(Double.parseDouble(accountBalanceText.getText()));
						account.setType(accountTypeSelect.getSelectedItem().toString());
						((SavingAccount) account).setBeginDate(dateFormat.parse(accountBeginTimeText.getText()));
						((SavingAccount) account).setEndDate(dateFormat.parse(accountEndTimeText.getText()));
						((SavingAccount) account).setInterest(Double.parseDouble(accountInterestText.getText()));
					}
					if (account != null) {
						bank.editAccount(account);
						accountsTable.getModel().setValueAt(account.getBalance() + "", selectedRow, 0);
						accountsTable.getModel().setValueAt(account.getHolderId() + "", selectedRow, 3);
						accountsTable.getModel().setValueAt(account.getId() + "", selectedRow, 4);
						accountsTable.getModel().setValueAt(account.getKey() + "", selectedRow, 6);
						accountsTable.getModel().setValueAt(account.getType(), selectedRow, 7);
						if (account instanceof SavingAccount) {
							accountsTable.getModel().setValueAt(dateFormat.format(((SavingAccount) account).getBeginDate()), selectedRow, 1);
							accountsTable.getModel().setValueAt(dateFormat.format(((SavingAccount) account).getEndDate()), selectedRow, 2);
							accountsTable.getModel().setValueAt(((SavingAccount) account).getInterest() + "", selectedRow, 5);
						}
					}
					panel.repaint();
				}
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
			}
		});

		editAccountButton.setBounds(150, 70, 100, 30);
		panel.add(editAccountButton);

		JButton deleteAccountButton = new JButton("Delete account");
		deleteAccountButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				super.mouseReleased(e);
				try {
					int selectedRow = accountsTable.getSelectedRow();
					if (selectedRow >= 0) {
						accountsTable.clearSelection();
						accountsTable.revalidate();
						accountsTable.repaint();
						((DefaultTableModel) accountsTable.getModel()).removeRow(accountsTable.convertRowIndexToModel(selectedRow));
						bank.deleteAccount(Integer.parseInt(accountsTable.getValueAt(selectedRow, 1).toString()));
					}
				} catch (Exception ex) {
					System.out.print("Error: " + ex.getMessage());
				}
			}
		});

		deleteAccountButton.setBounds(150, 110, 100, 30);
		panel.add(deleteAccountButton);
		panel.revalidate();
		panel.repaint();

	}
}
