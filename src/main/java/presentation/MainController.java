package presentation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class MainController extends JPanel {
	protected static final JFrame personFrame = new JFrame("Persons");//fereastra persoane
	protected static final JFrame accountFrame = new JFrame("Accounts");//fereastra conturi
	protected static final JFrame bankFrame = new JFrame("Bank");//fereastra banca

	public MainController() {
		super();
	}

	/**
	 * Generic method to create a table
	 *
	 * @param objects - the objects from which we extract the table headers and data
	 * @return - a new table
	 */
	protected JTable createTable(List<?> objects) {
		String[] tableHeaders = null;
		String[][] tableValues = null;
		if (objects != null && !objects.isEmpty()) {
			int propCount = 0;
			int column = 0;
			List<Method> getMethods = new LinkedList<>();
			for (Object object : objects) {
				Method[] methods = object.getClass().getMethods();
				methods = Arrays.stream(methods).sorted(Comparator.comparing(Method::getName)).collect(Collectors.toList()).toArray(new Method[methods.length]);
				for (Method method : methods) {
					if (method.getName().contains("get") && !method.getName().contains("getClass") && !getMethods.contains(method)) {
						propCount++;
						getMethods.add(method);
					}
				}
			}

			tableHeaders = new String[propCount];
			tableValues = new String[objects.size()][propCount];
			for (Object object : objects) {
				for (int i = 0; i < getMethods.size(); i++) {
					Method method = getMethods.get(i);
					if (method.getName().contains("get")) {
						method.setAccessible(true);
						tableHeaders[i] = method.getName().replace("get", "");
					}
				}

				for (int i = 0; i < getMethods.size(); i++) {
					Method method = getMethods.get(i);
					if (method.getName().contains("get")) {
						method.setAccessible(true);
						try {
							if ((object.getClass().equals(method.getDeclaringClass()) || object.getClass().getSuperclass().equals(method.getDeclaringClass())) && method.invoke(object) != null) {
								if (method.getReturnType().equals(Date.class)) {
									DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
									tableValues[column][i] = dateFormat.format(method.invoke(object));
								} else {
									tableValues[column][i] = method.invoke(object) + "";
								}
							} else {
								tableValues[column][i] = "-";
							}
						} catch (IllegalAccessException | InvocationTargetException e) {
							e.printStackTrace();
						}
					}
				}
				column++;
			}
		}

		if (tableValues != null) {
			return new JTable(new DefaultTableModel(tableValues, tableHeaders));
		} else {
			return null;
		}
	}

	/**
	 * Metoda care afiseaza fereastra principala
	 */
	private static void createAndShowGUI() {
		//Add content to the window.
		bankFrame.setLayout(null);
		bankFrame.add(new BankView());
		bankFrame.setSize(new Dimension(300, 300));
		bankFrame.setVisible(true);
	}

	public static void main(String[] args) {

		//Schedule a job for the event dispatch thread:
		//creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				//Turn off metal's use of bold fonts
				UIManager.put("swing.boldMetal", Boolean.FALSE);
				createAndShowGUI();
			}
		});
	}
}