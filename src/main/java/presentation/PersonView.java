package presentation;

import businessLayer.Bank;
import model.Person;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Radu Bompa
 * 5/2/2018
 */
public class PersonView extends MainController {

	public PersonView() {
		super();

		JComponent panel = new JPanel();
		panel.setName("personView");
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		panel.setAlignmentY(JPanel.TOP_ALIGNMENT);
		panel.setLayout(null);
		panel.setBounds(0, 0, 300, 600);

		Container pane = personFrame.getContentPane();
		pane.add(panel);
		final GridBagConstraints c = new GridBagConstraints();

		Bank bank = Bank.getInstance();
		//      person id text
		JLabel personIdLabel = new JLabel("Person id");
		JTextField personIdText = new JTextField();
		personIdText.setColumns(30);
		personIdText.setEditable(false);

		personIdLabel.setBounds(0, 30, 170, 20);
		panel.add(personIdLabel);
		personIdText.setBounds(0, 55, 100, 20);
		panel.add(personIdText);

		//      person name text
		JLabel personNameLabel = new JLabel("Person name");
		JTextField personNameText = new JTextField();
		personNameText.setColumns(30);

		personNameLabel.setBounds(0, 75, 170, 20);
		panel.add(personNameLabel);
		personNameText.setBounds(0, 100, 100, 20);
		panel.add(personNameText);

//		all persons table

		Set<Person> personList = bank.getAllPersons();
		if (personList.isEmpty()) {
			personList.add(new Person());
		}
		JTable personsTable = createTable(bank.getAllPersons().stream().collect(Collectors.toList()));

		personsTable.getSelectionModel().addListSelectionListener(event -> {
			if (personsTable.getSelectedRow() >= 0 && personsTable.getValueAt(personsTable.getSelectedRow(), 1) != null && personsTable.getValueAt(personsTable.getSelectedRow(), 1) != "") {
				Person person = new Person();
				person.setId(Integer.parseInt(personsTable.getValueAt(personsTable.getSelectedRow(), 0).toString()));
				person = bank.getPerson(person.getId());
				if (person != null) {
					personIdText.setText(person.getId() + "");
					personNameText.setText(person.getName());
				}
			} else {
				personNameText.setText("");
				personIdText.setText("");
			}
		});

		JScrollPane sp = new JScrollPane(personsTable);
		sp.setBounds(0, 220, 300, 300);
		panel.add(sp);

//      add, edit, delete buttons
		JButton addPersonButton = new JButton("Add person");
		addPersonButton.addActionListener(e -> {
			try {
				Person person = new Person();
				person.setName(personNameText.getText());
				int personId = bank.addPerson(person);
				((DefaultTableModel) personsTable.getModel()).addRow(new Object[]{personId+"", person.getName()});
				panel.repaint();
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
			}
		});

		addPersonButton.setBounds(150, 30, 100, 30);
		panel.add(addPersonButton);

		JButton editPersonButton = new JButton("Edit person");
		editPersonButton.addActionListener(e -> {
			try {
				int selectedRow = personsTable.getSelectedRow();
				if (personsTable.getValueAt(selectedRow, 1) != "") {
					Person person = new Person();
					person.setId(Integer.parseInt(personsTable.getValueAt(selectedRow, 0).toString()));
					person.setName(personNameText.getText());
					bank.editPerson(person);
					personsTable.getModel().setValueAt(person.getName(), selectedRow, 1);
					panel.repaint();
				}
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
			}
		});

		editPersonButton.setBounds(150, 70, 100, 30);
		panel.add(editPersonButton);

		JButton deletePersonButton = new JButton("Delete person");
		deletePersonButton.addActionListener(e -> {
			try {
				int selectedRow = personsTable.getSelectedRow();
				if (selectedRow >= 0) {
					personsTable.clearSelection();
					personsTable.revalidate();
					personsTable.repaint();
					Person person = new Person();
					person.setId(Integer.parseInt(personsTable.getValueAt(selectedRow, 0).toString()));
					((DefaultTableModel) personsTable.getModel()).removeRow(personsTable.convertRowIndexToModel(selectedRow));
					bank.deletePerson(person.getId());
				}
			} catch (Exception ex) {
				System.out.println("Error: " + ex.getMessage());
			}
		});

		deletePersonButton.setBounds(150, 110, 100, 30);
		panel.add(deletePersonButton);
	}
}
