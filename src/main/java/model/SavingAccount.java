package model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Radu Bompa
 * 5/10/2018
 */
public class SavingAccount extends Account implements Serializable {
	private Date beginDate;
	private Date endDate;
	private double interest;

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}
}
