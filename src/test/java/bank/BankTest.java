package bank;

import businessLayer.Bank;
import model.Account;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;

/**
 * Created by Radu Bompa
 * 5/10/2018
 */
public class BankTest {
	private Bank bank;
	@Before
	public void init() {
		bank = Bank.getInstance();
	}

	@Test
	public void testAddPerson() {
		Person person = new Person();
		person.setName("Vasile");
		int personId = bank.addPerson(person);
		assertNotNull(bank.getPerson(personId));
		assertEquals("Vasile", bank.getPerson(personId).getName());
	}

	@Test
	public void testEditPerson() {
		Person person = new Person();
		person.setName("Vasile");
		int personId = bank.addPerson(person);
		Person editPerson = bank.getPerson(personId);
		assertNotNull(editPerson);
		editPerson.setName("Vasile edit");
		bank.editPerson(editPerson);
		assertEquals("Vasile edit", bank.getPerson(personId).getName());
	}

	@Test
	public void testDeletePerson() {
		Person person = new Person();
		person.setName("Vasile");
		int personId = bank.addPerson(person);
		Person deletePerson = bank.getPerson(personId);
		assertNotNull(deletePerson);
		bank.deletePerson(deletePerson.getId());
		assertNull(bank.getPerson(deletePerson.getId()));
	}

	@Test
	public void testAddSavingAccount() {
		Person person = new Person();
		person.setName("Vasile");
		int personId = bank.addPerson(person);
		Person accPerson = bank.getPerson(personId);
		assertNotNull(accPerson);
		Account account = new SavingAccount();
		account.setHolderId(personId);
		account.setBalance(123.22);
		account.setType("SAVING");
		Date date = new Date();
		((SavingAccount)account).setBeginDate(date);
		((SavingAccount)account).setEndDate(new Date(date.getTime() + 1000));
		((SavingAccount)account).setInterest(3.2);
		int accountId = bank.addAccount(account);
		assertNotNull(bank.getAccount(accountId));
		assertEquals(123.22, bank.getAccount(accountId).getBalance());
		assertEquals(personId, bank.getAccount(accountId).getHolderId());
		assertEquals("SAVING", bank.getAccount(accountId).getType());
		assertEquals(date, ((SavingAccount) bank.getAccount(accountId)).getBeginDate());
		assertEquals(date.getTime() + 1000, ((SavingAccount) bank.getAccount(accountId)).getEndDate().getTime());
		assertEquals(3.2, ((SavingAccount) bank.getAccount(accountId)).getInterest());
	}

	@Test
	public void testAddSpendingAccount() {
		Person person = new Person();
		person.setName("Vasile");
		int personId = bank.addPerson(person);
		Person accPerson = bank.getPerson(personId);
		assertNotNull(accPerson);
		Account account = new SpendingAccount();
		account.setHolderId(personId);
		account.setBalance(123.22);
		account.setType("SAVING");
		Date date = new Date();
		int accountId = bank.addAccount(account);
		assertNotNull(bank.getAccount(accountId));
		assertEquals(123.22, bank.getAccount(accountId).getBalance());
		assertEquals(personId, bank.getAccount(accountId).getHolderId());
		assertEquals("SAVING", bank.getAccount(accountId).getType());
	}

	@Test
	public void testEditAccount() {
		Person person = new Person();
		person.setName("Vasile");
		int personId = bank.addPerson(person);
		Person accPerson = bank.getPerson(personId);
		assertNotNull(accPerson);
		Account account = new SpendingAccount();
		account.setHolderId(personId);
		account.setBalance(123.22);
		account.setType("SAVING");
		Date date = new Date();
		int accountId = bank.addAccount(account);
		assertNotNull(bank.getAccount(accountId));
		assertEquals(123.22, bank.getAccount(accountId).getBalance());
		assertEquals(personId, bank.getAccount(accountId).getHolderId());
		assertEquals("SAVING", bank.getAccount(accountId).getType());

		Account editAccount = bank.getAccount(accountId);
		editAccount.setBalance(122.01);
		bank.editAccount(editAccount);
		assertNotNull(bank.getAccount(accountId));
		assertEquals(122.01, bank.getAccount(accountId).getBalance());
	}

	@Test
	public void testDeleteAccount() {
		Person person = new Person();
		person.setName("Vasile");
		int personId = bank.addPerson(person);
		Person accPerson = bank.getPerson(personId);
		assertNotNull(accPerson);
		Account account = new SpendingAccount();
		account.setHolderId(personId);
		account.setBalance(123.22);
		account.setType("SAVING");
		Date date = new Date();
		int accountId = bank.addAccount(account);
		assertNotNull(bank.getAccount(accountId));
		assertEquals(123.22, bank.getAccount(accountId).getBalance());
		assertEquals(personId, bank.getAccount(accountId).getHolderId());
		assertEquals("SAVING", bank.getAccount(accountId).getType());

		Account deleteAccount = bank.getAccount(accountId);
		bank.deleteAccount(accountId);
		assertNull(bank.getAccount(accountId));
	}

	@Test
	public void testDepositIntoAccount() {
		Person person = new Person();
		person.setName("Vasile");
		int personId = bank.addPerson(person);
		Person accPerson = bank.getPerson(personId);
		assertNotNull(accPerson);
		Account account = new SpendingAccount();
		account.setHolderId(personId);
		account.setBalance(123.22);
		account.setType("SAVING");
		Date date = new Date();
		int accountId = bank.addAccount(account);
		assertNotNull(bank.getAccount(accountId));
		assertEquals(123.22, bank.getAccount(accountId).getBalance());
		assertEquals(personId, bank.getAccount(accountId).getHolderId());
		assertEquals("SAVING", bank.getAccount(accountId).getType());

		Account editAccount = bank.getAccount(accountId);
		bank.depositMoney(editAccount, 10.00);
		assertNotNull(bank.getAccount(accountId));
		assertEquals(133.22, bank.getAccount(accountId).getBalance());
	}

	@Test
	public void testWithdrawFromAccount() {
		Person person = new Person();
		person.setName("Vasile");
		int personId = bank.addPerson(person);
		Person accPerson = bank.getPerson(personId);
		assertNotNull(accPerson);
		Account account = new SpendingAccount();
		account.setHolderId(personId);
		account.setBalance(123.22);
		account.setType("SAVING");
		Date date = new Date();
		int accountId = bank.addAccount(account);
		assertNotNull(bank.getAccount(accountId));
		assertEquals(123.22, bank.getAccount(accountId).getBalance());
		assertEquals(personId, bank.getAccount(accountId).getHolderId());
		assertEquals("SAVING", bank.getAccount(accountId).getType());

		Account editAccount = bank.getAccount(accountId);
		bank.withdrawMoney(editAccount, 10.00);
		assertNotNull(bank.getAccount(accountId));
		assertEquals(113.22, bank.getAccount(accountId).getBalance());
	}
}
